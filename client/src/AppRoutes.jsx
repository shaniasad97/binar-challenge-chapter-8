import { Routes, Route } from 'react-router-dom';
import Navigation from './Components/Navbar';
import Home from './Pages/Home'
import Create from './Pages/Create'
import DetailCreate from './Pages/DetailCreate'
import Update from './Pages/Update'
import DetailUpdate from './Pages/DetailUpdate'
import Search from './Pages/Search'
import DetailSearch from './Pages/DetailSearch'

export default function AppRoutes() {
    return (
        <>
            <Navigation/>
            <Routes>
                <Route path="/" element={< Home />} />
                <Route path="/create" element={< Create />} />
                <Route path="/detail/create" element={< DetailCreate />} />
                <Route path="/update" element={< Update />} />
                <Route path="/detail/update" element={< DetailUpdate />} />
                <Route path="/search" element={< Search />} />
                <Route path="/detail/search" element={< DetailSearch />} />
            </Routes>
        </>
    )
}