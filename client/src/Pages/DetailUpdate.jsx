import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';

import {
    useLocation
} from "react-router-dom";

function DetailUpdate(props) {
    var { name, gameName } = useLocation().state
    return (
        <Container>
            <Table striped bordered hover size="sm" className={'mt-3'}>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Game Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{name}</td>
                    <td>{gameName}</td>
                </tr>
            </tbody>
        </Table>
        </Container>
    )
}

export default DetailUpdate