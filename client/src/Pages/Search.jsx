import { useState } from 'react'
import { Navigate } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';


const Search = () => {
    const [submitted, setSubmitted] = useState(false);
    const [gameName, setGameName] = useState("");



    if (submitted) {
        return < Navigate
            to="/detail/search"
            replace={true}
            state={{
                gameName: gameName
            }}
        />
    }


    return (
        <Container>
            <Row style={{ height: '680px' }}>
                <Col className='p-3'>
                    <div className='d-flex flex-row-reverse'>
                        <img className='bg float-right' src="bg2.jpg" alt='bg' />
                    </div>
                </Col>
                <Col className='p-3'>
                    <Form style={{ width: "70%" }} onSubmit={() => setSubmitted(true)}>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
                            <Form.Label>Game Name</Form.Label>
                            <Form.Control required value={gameName} type="text" onChange={(e) => setGameName(e.target.value)} placeholder="Your game name" />
                        </Form.Group>

                        <Button variant="dark" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default Search